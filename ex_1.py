def addition(a,b):
        return a + b

def soustraction(a,b):
        return a - b

def produit(a,b):
        return a*b

def quotient(a,b):
    if b!= 0:
        return a/b
    else:
        return 'pas de division par 0'


a = 23
b = 5
print(addition(a,b))
print(soustraction(a,b))
print(produit(a,b))
print(quotient(a,b))
